class Image < ApplicationRecord
  validates_presence_of :name, :album_prefix, :extension
  belongs_to :album, foreign_key: :album_prefix, primary_key: :prefix


  def extension_format
    if self.extension.blank? || (self.extension.strip.downcase =~ /\A(jpg|jpeg|png)\z/).blank?
      self.errors.add(:base, "Only jpg, jpeg, png extensions are allowed")
    end
  end

  def generate_version_url(version)
    return  Config::VERSION_IMAGES_BASE_URL + Config::VERSION_IMAGES_BUCKET + "/#{self.album_prefix}/#{self.name}/#{version}.#{self.extension}"
  end

end
  