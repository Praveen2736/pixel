class ImagesController < ApplicationController

  def create
    _, bucket, prefix, file_name, version = URI.decode(params["filepath"]).split("/")
    version_name, extension = version.split(".")
    image = Image.new({album_prefix: prefix, name: file_name, extension: extension})
    if image.save
      status, message = 200, nil, "Successfully Saved"
    else
      status, message =  400, image.errors.full_messages
    end
    render json: {message: message}, status: status
  end

end
