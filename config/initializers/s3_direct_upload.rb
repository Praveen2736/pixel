S3DirectUpload.config do |c|
  c.access_key_id =  Config::ACCESS_KEY_ID      # your access key id
  c.secret_access_key = Config::SECRET_ACCESS_KEY   # your secret access key
  c.bucket = Config::ORIGINAL_IMAGES_BUCKET             # your bucket name
  c.region = Config::ORIGINAL_IMAGES_REGION           # region prefix of your bucket url. This is _required_ for the non-default AWS region, eg. "s3-eu-west-1"
end


