Rails.application.routes.draw do

  root to: "albums#index"

  resources :albums

  post 'albums/create-images', to: 'images#create'

  get 'albums/show-images/:id', to: 'albums#show_images', as: 'show_images'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
