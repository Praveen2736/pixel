class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.string :name
      t.string :album_prefix
      t.string :extension
      t.timestamps
    end
    add_index(:images, :album_prefix)

  end
end
