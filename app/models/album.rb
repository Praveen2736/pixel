class Album < ApplicationRecord
  before_validation :assign_prefix, on: :create
  validates_presence_of :name, :prefix
  has_many :images, foreign_key: :album_prefix, primary_key: :prefix

  def assign_prefix
    self.prefix = SecureRandom.hex(4)
  end


end
