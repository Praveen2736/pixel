class CreateAlbums < ActiveRecord::Migration[5.1]
  def change
    create_table :albums do |t|
      t.string :name
      t.string :prefix

      t.timestamps
    end

    add_index(:albums, :prefix, unique: true)

  end
end
